import os
import playsound
from gtts import gTTS

def tell(text : str):
    """
    Read the text out loud

    ## Args
    - *text*, **str**: text to be spoke
    """
    tts = gTTS(text, lang='fr')
    filename = "tmp.mp3"
    tts.save(filename)
    playsound.playsound(filename)
    os.remove(filename)
