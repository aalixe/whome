import subprocess
from typing import List, Dict
from multiprocessing import Pool, Array

def ping(last_digit) -> int:
    """
    Execute a ping command

    ### Return 
    - *pinged_addr*, **str**: the address that was pinged
    - *ping_command_return_value*, **int**: return value of the pinged command
    """
    pinged_addr = '192.168.0.' + str(last_digit)
    ping_command = ['ping', '-q', '-c', '1', '-W','1', pinged_addr]
    ping_command_return_value = subprocess.call(ping_command)
    return pinged_addr, ping_command_return_value

def get_connected_devices() -> List:
    """
    List address of connected devices on local network
    This function use multiples processes to speed up ping routine

    ### Return
    - *connected_devices*, **List<str>**: the addresses str list of connected devices
    """
    connected_devices = []
    with Pool() as pool:
         list_ping_return_value = pool.map(ping, range(2, 254))
    accepted_ping = list(filter(lambda x: not x[1], list_ping_return_value))
    return list(map(lambda x: x[0], accepted_ping))


def get_name_from_addr(addr : str) -> str:
    """
    Retrive hostname from an IP address

    ### Return
    - *Name*, **str**: the hostname
    """
    nslookup_command = ['nslookup', addr]
    nslookup_proc = subprocess.Popen(nslookup_command, stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    try:
        outs, errs = nslookup_proc.communicate(timeout=15)
    except subprocess.TimeoutExpired:
        nslookup_proc.kill()
        return "Unknown"
    decoded_output = outs.decode()
    if nslookup_proc.returncode:
        return "Unknown"
    return outs.decode().split("\n")[0].split("=")[1][1:-1]

def get_names_of_connected_devices(devices_list : List) -> List:
    """
    Retrieve all devices names from IP adress list
    """
    return list(map(lambda device_addr: get_name_from_addr(device_addr), devices_list))

def list_connected_devices() -> Dict[str, str]:
    """
    List all connected devices on network. Names and addresse

    ## Return
    - *currently_connected_devices*, **Dict[str, str]**: dictionnary<Name, IP> currently connected
    """
    connected_devices = get_connected_devices()
    names = get_names_of_connected_devices(connected_devices)
    currently_connected_devices_info = dict(zip(names, connected_devices))
    return currently_connected_devices_info