from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse

from whome.devices import list_connected_devices

app = FastAPI()

templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")


@app.get("/dashboard")
def dashboard(request: Request):
    return templates.TemplateResponse("base.html", {"request": request, "connected_devices": list_connected_devices()})